#include "Tracer.h" // <1>

Tracer::Tracer():
  leftWheel(PORT_C), rightWheel(PORT_B), colorSensor(PORT_3), touchSensor(PORT_4) { // <2>
}

void Tracer::init() 
{
  rightWheel.reset();
  leftWheel.reset();
  init_f("Tracer");
  // ev3_sdcard_opendir("test.txt");
  fp = fopen("test.txt","w");
  fputs("test\n",fp);
  fclose(fp);
}

void Tracer::terminate() {
  msg_f("Stopped.", 1);
  leftWheel.stop();  // <1>
  rightWheel.stop();
}

void Tracer::run() {

  // int section = parameterTable[0].section;
  // colorSensorで明度を取る
  int brightness = colorSensor.getBrightness();
  int diff = brightness - parameterTable[sectionnum].target;
  // Log
  // msg_f("running...", 1);
  // sprintf(buf,"diff=%d", diff);
  // _log(buf);

  diff_Val[1] = diff_Val[0];         // 前回の偏差
  diff_Val[0] = diff;                // 今回の偏差
  cumulative_Val += ( diff_Val[0] + diff_Val[1]) / 2.0 * DELTA_T; // 偏差の累積

  float p = parameterTable[sectionnum].Kp * diff;                                  // 比例
  float i = parameterTable[sectionnum].Ki * cumulative_Val;                        // 積分
  float d = parameterTable[sectionnum].Kd * (diff_Val[0] - diff_Val[1]) / DELTA_T; // 微分

  float  turn = (p+i+d) + parameterTable[sectionnum].bias; // targetより明るいと＋, 暗いと－

  // ライン進行方向に対して左のエッジ
  // 明るい(白いと) 左車輪を＋ 右車輪を－
  // 暗い(黒いと) 左車輪を－ 右車輪を＋
  leftWheel_counts = leftWheel.getCount();
  rightWheel_counts = rightWheel.getCount();
  
  
  leftWheel.setPWM(parameterTable[sectionnum].pwm + turn); 
  rightWheel.setPWM(parameterTable[sectionnum].pwm - turn);
  touch();
}
// touchSensor.isPressed()

// スペースでその地点の回転数をターミナルに表示
void Tracer::touch() {
  if(touchSensor.isPressed() && sw == 0){
    fp = fopen("test.txt","a");
    sprintf(buf,"{       %d,            %d,             %d, 19,  50, 1.98, 0.00007, 0.17,  19,  0 }, //%d\n",sectionput, leftWheel_counts, rightWheel_counts,sectionput);
    _log(buf);
    sectionput++;
    
    // fprintf(fp, buf,"%d,%d", leftWheel_counts, rightWheel_counts);
    // fwrite()
    fputs(buf,fp);
    // fflush(fp);
    fclose(fp);
    
    sw = 1;
  }else if( !touchSensor.isPressed() && sw == 1 ){
    sw = 0;
  }
} 
//区間の確認、変更を行う（平均を取らずに両方の値が全て超えたら次の区間に変更）
void Tracer::sectioncheck(){
    if (parameterTable[sectionnum].leftupperlimit < leftWheel_counts && parameterTable[sectionnum].rightupperlimit < rightWheel_counts){
        sectionnum++;
        sprintf(buf,"*start:%d data:%d", sectionnum, parameterTable[sectionnum].section);
        _log(buf);
        // init();
    }

}

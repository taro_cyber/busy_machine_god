// #include "Tracer.h"

class Section
{
private:
    /* data */
    int sectionnum = 0;
    PARAMETER parameterTable[9] = {
        //section, leftupperlimit, rightupperlimit, mThreshold, pwm, Kp, Ki, Kd, target, bias,
        {       0,            360,             360, 19,  40, 1.98, 0.00007, 0.17,  19,  0 }, // 0
        {       1,           1921,            1918, 19,  95, 1.98, 0.00007, 0.17,  19,  0 }, // 1
        {       2,           3265,            2987, 19,  70, 1.98, 0.00007, 0.17,  19,  0 }, // 2
        {       3,           3668,            3394, 19,  65, 1.98, 0.00007, 0.17,  19,  0 }, // 3
        {       4,          10290,           10018, 19,  55, 1.98, 0.00007, 0.16,  19,  0 }, // 4
        {       5,          12573,           12304, 19,  95, 1.98, 0.00007, 0.17,  19,  0 }, // 5
        {       6,          20000,           20000, 19,   0, 1.98, 0.00007, 0.17,  19,  0 }, // 6
        // {       6,           8633,           8084, 19,  40, 1.98, 0.00007, 0.17,  19,  0 }, // 0
        // {       7,          10175,           9869, 19,  40, 1.98, 0.00007, 0.17,  19,  0 }, // 0
        // {       8,          20000,          20000, 19,  40, 1.98, 0.00007, 0.17,  19,  0 }, // 0
    };

public:
    Section();
    void sectioncheck();
    void parameteredit();

    
    // void sectiondisplay();


// Section::Section()leftWheel(PORT_C), rightWheel(PORT_B), colorSensor(PORT_3), touchSensor(PORT_4) { // <2>
// }

// void sectioncheck(){
//     if (parameterTable[sectionnum].leftupperlimit > leftWheel_counts && parameterTable[sectionnum].rightupperlimit > rightWheel_counts){
//         sectionnum++;
//     }

// }
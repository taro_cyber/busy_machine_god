
#ifndef TYPE_PARAMETER
#define TYPE_PARAMETER

typedef struct _PARAMETER {
    int     section;
    int     leftupperlimit;
    int     rightupperlimit;
    int     mThreshold;
    int     pwm;
    float   Kp;
    float   Ki;
    float   Kd;
    int8_t  target;
    int8_t  bias;
}  PARAMETER;
#endif
